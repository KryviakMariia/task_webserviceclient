package com.kryviak.service.rest;

import com.kryviak.service.Constants;
import com.kryviak.service.NewsPaperFactory;
import com.kryviak.service.model.NewsPaperModel;
import com.kryviak.service.bo.NewsPaperBO;
import com.kryviak.service.rest.client.ClientService;
import org.apache.http.HttpResponse;
import org.testng.Assert;
import org.testng.annotations.Test;

public class PutNewsTest {

    @Test
    public void testChangeNews() {
        NewsPaperBO newsPaperBO = new NewsPaperBO();
        NewsPaperModel model = new NewsPaperFactory().getNewsPaperModel();
        model.setTitle("t1");
        HttpResponse responsePut = new ClientService().sendPutRequest(Constants.EDIT_NEWS_ENDPOINT, model);
        HttpResponse responseGetAllNews = new ClientService().sendGetRequest(Constants.GET_ALL_ENDPOINT);
        Assert.assertEquals(202, responsePut.getStatusLine().getStatusCode(), "Status Code should be 202");
        Assert.assertTrue(newsPaperBO.isNewsChanged(newsPaperBO.getAllNews(responseGetAllNews), model),
                "News should be changed.");
    }
}

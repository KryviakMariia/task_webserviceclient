package com.kryviak.service.rest;

import com.kryviak.service.Constants;
import com.kryviak.service.bo.NewsPaperBO;
import com.kryviak.service.rest.client.ClientService;
import org.apache.http.HttpResponse;
import org.testng.Assert;
import org.testng.annotations.Test;

public class DeleteNewsTest {

    @Test
    public void testDeleteNews() {
        NewsPaperBO newsPaperBO = new NewsPaperBO();
        String title = "t1";
        HttpResponse responseDelete = new ClientService().sendDeleteRequest(Constants.DELETE_NEWS_ENDPOINT + title);
        HttpResponse responseGetAllNews = new ClientService().sendGetRequest(Constants.GET_ALL_ENDPOINT);

        Assert.assertEquals(202, responseDelete.getStatusLine().getStatusCode(), "Status Code should be 202");
        Assert.assertFalse(newsPaperBO.isNewsPresent(newsPaperBO.getAllNews(responseGetAllNews), title), "News with title: '" + title
                + "' should be deleted from database.");
    }
}

package com.kryviak.service.rest;

import com.kryviak.service.Constants;
import com.kryviak.service.model.NewsPaperModel;
import com.kryviak.service.bo.NewsPaperBO;
import com.kryviak.service.rest.client.ClientService;
import org.apache.http.HttpResponse;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

public class GetAllNewsTest {

    @Test
    public void testGetAllNews() {
        HttpResponse response = new ClientService().sendGetRequest(Constants.GET_ALL_ENDPOINT);
        List<NewsPaperModel> newsPaperModels = new NewsPaperBO().getAllNews(response);
        Assert.assertNotNull(newsPaperModels, "Get All endpoint should return News Paper list.");
        Assert.assertEquals(200, response.getStatusLine().getStatusCode(), "Status Code should be 200");
    }
}

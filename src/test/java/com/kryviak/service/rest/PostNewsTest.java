package com.kryviak.service.rest;

import com.kryviak.service.Constants;
import com.kryviak.service.NewsPaperFactory;
import com.kryviak.service.model.NewsPaperModel;
import com.kryviak.service.bo.NewsPaperBO;
import com.kryviak.service.rest.client.ClientService;
import org.apache.http.HttpResponse;
import org.testng.Assert;
import org.testng.annotations.Test;

public class PostNewsTest {

    @Test
    public void testAddNews() {
        NewsPaperBO newsPaperBO = new NewsPaperBO();
        NewsPaperModel model = new NewsPaperFactory().getNewsPaperModel();
        model.setTitle("t1");
        model.setGenre("g1");
        HttpResponse responsePost = new ClientService().sendPostRequest(Constants.ADD_NEWS_ENDPOINT, model);
        HttpResponse responseGetAllNews = new ClientService().sendGetRequest(Constants.GET_ALL_ENDPOINT);
        Assert.assertEquals(201, responsePost.getStatusLine().getStatusCode(), "Status Code should be 201");
        Assert.assertTrue(newsPaperBO.isNewsPresent(newsPaperBO.getAllNews(responseGetAllNews), model.getTitle()),
                "News should be added to database.");
    }
}

package com.kryviak.service.soap;

import com.kryviak.service.NewsPaperFactory;
import com.kryviak.service.bo.NewsPaperBO;
import com.kryviak.service.model.NewsPaperModel;
import com.kryviak.service.soap.client.NewsPaperSoapServiceImplService;
import org.testng.Assert;
import org.testng.annotations.Test;

public class PutNewsTest {

    @Test
    public void testChangeNews() {
        NewsPaperBO newsPaperBO = new NewsPaperBO();
        NewsPaperSoapServiceImplService service = new NewsPaperSoapServiceImplService();
        NewsPaperModel newsPaperModel = new NewsPaperFactory().getNewsPaperModel();
        newsPaperModel.setTitle("t1");
        service.getNewsPaperSoapServiceImplPort().changeNews(newsPaperModel);
        Assert.assertTrue(newsPaperBO.isNewsChanged(service.getNewsPaperSoapServiceImplPort().getAllNews(), newsPaperModel),
                "News should be changed.");
    }
}

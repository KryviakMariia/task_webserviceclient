package com.kryviak.service.soap;

import com.kryviak.service.bo.NewsPaperBO;
import com.kryviak.service.soap.client.NewsPaperSoapServiceImplService;
import org.testng.Assert;
import org.testng.annotations.Test;

public class DeleteNewsTest {

    @Test
    public void testDeleteNews() {
        NewsPaperSoapServiceImplService service = new NewsPaperSoapServiceImplService();
        String title = "00e11fac-db84-46f9-895a-c68d92cdf95b";
        service.getNewsPaperSoapServiceImplPort().removeNews(title);
        NewsPaperBO newsPaperBO = new NewsPaperBO();
        Assert.assertFalse(newsPaperBO.isNewsPresent(service.getNewsPaperSoapServiceImplPort().getAllNews(), title), "News should be deleted from database.");
    }
}

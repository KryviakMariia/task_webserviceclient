package com.kryviak.service.soap;

import com.kryviak.service.model.NewsPaperModel;
import com.kryviak.service.soap.client.NewsPaperSoapServiceImplService;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

public class GetNewsByGenreTest {

    @Test
    public void testGetAllNewsByGenre() {
        NewsPaperSoapServiceImplService service = new NewsPaperSoapServiceImplService();
        List<NewsPaperModel> newsPaperModels = service.getNewsPaperSoapServiceImplPort().getNews("g9");
        Assert.assertNotNull(newsPaperModels, "Get All genre method should return News Paper list.");
    }
}

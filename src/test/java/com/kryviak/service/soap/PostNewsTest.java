package com.kryviak.service.soap;

import com.kryviak.service.NewsPaperFactory;
import com.kryviak.service.bo.NewsPaperBO;
import com.kryviak.service.model.NewsPaperModel;
import com.kryviak.service.soap.client.NewsPaperSoapServiceImplService;
import org.testng.Assert;
import org.testng.annotations.Test;

public class PostNewsTest {

    @Test
    public void testAddNews() {
        NewsPaperBO newsPaperBO = new NewsPaperBO();
        NewsPaperModel newsPaperModel = new NewsPaperFactory().getNewsPaperModel();
        NewsPaperSoapServiceImplService service = new NewsPaperSoapServiceImplService();
        service.getNewsPaperSoapServiceImplPort().addNews(newsPaperModel);
        Assert.assertTrue(newsPaperBO.isNewsPresent(service.getNewsPaperSoapServiceImplPort().getAllNews(), newsPaperModel.getTitle()),
                "News should be added to database.");
    }
}

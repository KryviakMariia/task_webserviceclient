package com.kryviak.service.soap;

import com.kryviak.service.model.NewsPaperModel;
import com.kryviak.service.soap.client.NewsPaperSoapServiceImplService;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

public class GetAllNewsTest {

    @Test
    public void testGetAllNews() {
        NewsPaperSoapServiceImplService service = new NewsPaperSoapServiceImplService();
        List<NewsPaperModel> newsPaperModels = service.getNewsPaperSoapServiceImplPort().getAllNews();
        Assert.assertNotNull(newsPaperModels, "Get All method should return News Paper list.");
    }
}

package com.kryviak.service.bo;

import com.kryviak.service.model.NewsPaperModel;
import org.apache.http.HttpResponse;
import org.apache.http.impl.client.BasicResponseHandler;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.List;

public class NewsPaperBO {

    private static Logger logger = LogManager.getLogger(NewsPaperBO.class);

    public List<NewsPaperModel> getAllNews(HttpResponse response) {
        String result = null;
        List<NewsPaperModel> newsPaperModels = null;
        try {
            result = new BasicResponseHandler().handleResponse(response);
            ObjectMapper mapper = new ObjectMapper();
            newsPaperModels = mapper.readValue(result, new TypeReference<List<NewsPaperModel>>() {
            });
            logger.info("Map result to NewsPaper model");
        } catch (IOException e) {
            logger.error(e);
        }
        logger.info("Get all newspapers");
        return newsPaperModels;
    }

    public boolean isNewsPresent(List<NewsPaperModel> newsPaperModels, String title) {
        boolean flag = false;
        for (NewsPaperModel newsPaperModel : newsPaperModels) {
            if (newsPaperModel.getTitle().equalsIgnoreCase(title)) {
                flag = true;
            }
        }
        if (flag){
            logger.info("NewsPaper with title: '" + title + "' is present in database");
        } else {
            logger.info("NewsPaper with title: '" + title + "' is not present in database");
        }
        return flag;
    }

    public boolean isNewsChanged(List<NewsPaperModel> newsPaperModels, NewsPaperModel model) {
        boolean flag = false;
        for (NewsPaperModel newsPaperModel : newsPaperModels) {
            if (newsPaperModel.getGenre().equalsIgnoreCase(model.getGenre()) && newsPaperModel.getMessageLink().equalsIgnoreCase(model.getMessageLink())) {
                flag = true;
            }
        }
        if (flag){
            logger.info("NewsPaper with title: '" + model.getTitle() + "' was changed");
        } else {
            logger.info("NewsPaper with title: '" + model.getTitle() + "' was not changed");
        }
        return flag;
    }
}

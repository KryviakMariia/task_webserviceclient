package com.kryviak.service.rest.client;

import com.kryviak.service.model.NewsPaperModel;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class ClientService {

    private static Logger logger = LogManager.getLogger(ClientService.class);

    public HttpResponse sendGetRequest(String url) {
        HttpResponse response = null;
        HttpGet request = new HttpGet(url);
        try {
            HttpClient client = HttpClientBuilder.create().build();
            response = client.execute(request);
        } catch (IOException e) {
            logger.error(e);
        }
        logger.info("Send GET request. Get all news");
        return response;
    }

    public HttpResponse sendPostRequest(String url, NewsPaperModel model) {
        HttpResponse response = null;
        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpPost request = new HttpPost(url);
        request.addHeader("content-type", "application/json");
        request.setEntity(new StringEntity(model.toString(), StandardCharsets.UTF_8));
        try {
            response = httpClient.execute(request);
        } catch (IOException e) {
            logger.error(e);
        }
        logger.info("Send POST request. NewsPaper with the title: '" + model.getTitle() + "'");
        return response;
    }

    public HttpResponse sendDeleteRequest(String url) {
        HttpResponse response = null;
        HttpDelete request = new HttpDelete(url);
        try {
            HttpClient client = HttpClientBuilder.create().build();
            response = client.execute(request);
        } catch (IOException e) {
            logger.error(e);
        }
        logger.info("Send DELETE request");
        return response;
    }

    public HttpResponse sendPutRequest(String url, NewsPaperModel model) {
        HttpResponse response = null;
        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpPut request = new HttpPut(url);
        request.addHeader("content-type", "application/json");
        request.setEntity(new StringEntity(model.toString(), StandardCharsets.UTF_8));
        try {
            response = httpClient.execute(request);
        } catch (IOException e) {
            logger.error(e);
        }
        logger.info("Send PUT request. NewsPaper with the title: '" + model.getTitle() + "'");
        return response;
    }
}

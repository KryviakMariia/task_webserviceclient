package com.kryviak.service;

public class Constants {
    public final static String GET_ALL_ENDPOINT = "http://127.0.0.1/TaskService/rest/all";
    public final static String ADD_NEWS_ENDPOINT = "http://127.0.0.1/TaskService/rest/post/news";
    public final static String GET_ALL_GENRE_ENDPOINT = "http://127.0.0.1/TaskService/rest/get?genre=";
    public final static String DELETE_NEWS_ENDPOINT = "http://127.0.0.1/TaskService/rest/delete?title=";
    public final static String EDIT_NEWS_ENDPOINT = "http://127.0.0.1/TaskService/rest/edit";
}

package com.kryviak.service.soap.runner;


import com.kryviak.service.model.NewsPaperModel;
import com.kryviak.service.soap.client.NewsPaperSoapServiceImplService;

import java.util.List;

public class Main {

    public static void main(String [] x) {
        NewsPaperSoapServiceImplService service = new NewsPaperSoapServiceImplService();
        List<NewsPaperModel> items = service.getNewsPaperSoapServiceImplPort().getAllNews();

        items.forEach(i -> System.out.println(i.getTitle()));
    }
}

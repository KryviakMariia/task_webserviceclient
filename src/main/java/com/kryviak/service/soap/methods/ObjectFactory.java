
package com.kryviak.service.soap.methods;

import com.kryviak.service.model.NewsPaperModel;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.kryviak.service.soap package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _RemoveNewsResponse_QNAME = new QName("http://soap.service.kryviak.com/", "removeNewsResponse");
    private final static QName _GetNewsResponse_QNAME = new QName("http://soap.service.kryviak.com/", "getNewsResponse");
    private final static QName _RemoveNews_QNAME = new QName("http://soap.service.kryviak.com/", "removeNews");
    private final static QName _ChangeNews_QNAME = new QName("http://soap.service.kryviak.com/", "changeNews");
    private final static QName _GetNews_QNAME = new QName("http://soap.service.kryviak.com/", "getNews");
    private final static QName _GetAllNews_QNAME = new QName("http://soap.service.kryviak.com/", "getAllNews");
    private final static QName _GetAllNewsResponse_QNAME = new QName("http://soap.service.kryviak.com/", "getAllNewsResponse");
    private final static QName _AddNewsResponse_QNAME = new QName("http://soap.service.kryviak.com/", "addNewsResponse");
    private final static QName _ChangeNewsResponse_QNAME = new QName("http://soap.service.kryviak.com/", "changeNewsResponse");
    private final static QName _AddNews_QNAME = new QName("http://soap.service.kryviak.com/", "addNews");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.kryviak.service.soap
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ChangeNewsResponse }
     * 
     */
    public ChangeNewsResponse createChangeNewsResponse() {
        return new ChangeNewsResponse();
    }

    /**
     * Create an instance of {@link AddNews }
     * 
     */
    public AddNews createAddNews() {
        return new AddNews();
    }

    /**
     * Create an instance of {@link GetAllNews }
     * 
     */
    public GetAllNews createGetAllNews() {
        return new GetAllNews();
    }

    /**
     * Create an instance of {@link GetAllNewsResponse }
     * 
     */
    public GetAllNewsResponse createGetAllNewsResponse() {
        return new GetAllNewsResponse();
    }

    /**
     * Create an instance of {@link AddNewsResponse }
     * 
     */
    public AddNewsResponse createAddNewsResponse() {
        return new AddNewsResponse();
    }

    /**
     * Create an instance of {@link ChangeNews }
     * 
     */
    public ChangeNews createChangeNews() {
        return new ChangeNews();
    }

    /**
     * Create an instance of {@link GetNews }
     * 
     */
    public GetNews createGetNews() {
        return new GetNews();
    }

    /**
     * Create an instance of {@link RemoveNewsResponse }
     * 
     */
    public RemoveNewsResponse createRemoveNewsResponse() {
        return new RemoveNewsResponse();
    }

    /**
     * Create an instance of {@link GetNewsResponse }
     * 
     */
    public GetNewsResponse createGetNewsResponse() {
        return new GetNewsResponse();
    }

    /**
     * Create an instance of {@link RemoveNews }
     * 
     */
    public RemoveNews createRemoveNews() {
        return new RemoveNews();
    }

    /**
     * Create an instance of {@link NewsPaperModel }
     * 
     */
    public NewsPaperModel createNewsPaperModel() {
        return new NewsPaperModel();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveNewsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.service.kryviak.com/", name = "removeNewsResponse")
    public JAXBElement<RemoveNewsResponse> createRemoveNewsResponse(RemoveNewsResponse value) {
        return new JAXBElement<RemoveNewsResponse>(_RemoveNewsResponse_QNAME, RemoveNewsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetNewsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.service.kryviak.com/", name = "getNewsResponse")
    public JAXBElement<GetNewsResponse> createGetNewsResponse(GetNewsResponse value) {
        return new JAXBElement<GetNewsResponse>(_GetNewsResponse_QNAME, GetNewsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveNews }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.service.kryviak.com/", name = "removeNews")
    public JAXBElement<RemoveNews> createRemoveNews(RemoveNews value) {
        return new JAXBElement<RemoveNews>(_RemoveNews_QNAME, RemoveNews.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeNews }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.service.kryviak.com/", name = "changeNews")
    public JAXBElement<ChangeNews> createChangeNews(ChangeNews value) {
        return new JAXBElement<ChangeNews>(_ChangeNews_QNAME, ChangeNews.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetNews }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.service.kryviak.com/", name = "getNews")
    public JAXBElement<GetNews> createGetNews(GetNews value) {
        return new JAXBElement<GetNews>(_GetNews_QNAME, GetNews.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllNews }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.service.kryviak.com/", name = "getAllNews")
    public JAXBElement<GetAllNews> createGetAllNews(GetAllNews value) {
        return new JAXBElement<GetAllNews>(_GetAllNews_QNAME, GetAllNews.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllNewsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.service.kryviak.com/", name = "getAllNewsResponse")
    public JAXBElement<GetAllNewsResponse> createGetAllNewsResponse(GetAllNewsResponse value) {
        return new JAXBElement<GetAllNewsResponse>(_GetAllNewsResponse_QNAME, GetAllNewsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddNewsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.service.kryviak.com/", name = "addNewsResponse")
    public JAXBElement<AddNewsResponse> createAddNewsResponse(AddNewsResponse value) {
        return new JAXBElement<AddNewsResponse>(_AddNewsResponse_QNAME, AddNewsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeNewsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.service.kryviak.com/", name = "changeNewsResponse")
    public JAXBElement<ChangeNewsResponse> createChangeNewsResponse(ChangeNewsResponse value) {
        return new JAXBElement<ChangeNewsResponse>(_ChangeNewsResponse_QNAME, ChangeNewsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddNews }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.service.kryviak.com/", name = "addNews")
    public JAXBElement<AddNews> createAddNews(AddNews value) {
        return new JAXBElement<AddNews>(_AddNews_QNAME, AddNews.class, null, value);
    }

}

package com.kryviak.service;

import com.kryviak.service.model.NewsPaperModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.UUID;

public class NewsPaperFactory {

    private static Logger logger = LogManager.getLogger(NewsPaperFactory.class);

    public NewsPaperModel getNewsPaperModel() {
        NewsPaperModel model = new NewsPaperModel();
        model.setMessageLink(UUID.randomUUID().toString());
        model.setMessage(UUID.randomUUID().toString());
        model.setGenre(UUID.randomUUID().toString());
        model.setDescription(UUID.randomUUID().toString());
        model.setTitle(UUID.randomUUID().toString());
        logger.info("Set random data into NewsPaper model");
        return model;
    }
}
